import datetime
import copy
import math

import requests
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import AnonymousUser
from django.shortcuts import render

# Create your views here.
from first.forms import TokenForm

def calculateV(W, G):
  return W/(G + 192) * 5
def chek_k(ch_k):
    for t in range(0, 31):
        chekk = False
        for oxi in range(0, 61):
            k = -1 * math.cos((math.pi * (t + 0.5 * oxi)) / 40)
            if ch_k == 0:
                if k>=0 and k<0.1:
                    n_t = t
                    n_oxi = oxi
                    chekk = True
                    break
            elif ch_k == 1:
                if k == 1:
                    n_t = t
                    n_oxi = oxi
                    chekk = True
        if chekk:
            break

    e_t = 0
    for i in range(0, n_t+1):
        e_t += i

    return e_t, n_oxi

def temp(mn):
    en = mn*11
    it = None
    for i in range(0, 31):
        summ = 0
        for j in range(0, i+1):
            summ += j
            if summ == en:
                it = i
                break
        if it != None:
            break
    return it

def during_population(SH):
    start_count_SH = 8
    days_count = 0
    energy = 0
    dist = 0
    Oxi = 0
    while start_count_SH - 8 < SH:
        start_count_SH *= 2
        days_count += 1
        res = chek_k(1)
        Oxi += res[1]
        dist += calculateV(max(100 - res[0], 80), start_count_SH + 192)
        energy += (res[0] - 1) // 11 + 1 + max(100 - res[0], 80)


    return [dist, Oxi, energy, days_count]

def all_time(SH, distance):
    population_res = during_population(SH)
    days_count = population_res[3] + (distance - population_res[0])/2
    energy = population_res[3] + 80 * (distance - population_res[0])/2
    oxi = population_res[1] + 41 * (distance - population_res[0])/2

    return [days_count, energy, oxi]



def Main_page(request):
    context = {}

    if request.method == "POST":
        form = TokenForm(request.POST)
        if form.is_valid():
            token = form.cleaned_data['token']
            number_of_polet = form.cleaned_data['number']

            url = 'https://dt.miet.ru/ppo_it_final'
            headers = {'X-Auth-Token': token}
            response = requests.get(url, headers=headers)

            str = "".join(response.text)
            str = str.replace(" ", "").replace("points", " ").split()
            print(str)
            tmp = []
            for i in str:
                if "SH" in i:
                    tmp += [i.replace('"', "").replace(':', " ").replace('{', " ").replace('}', " ").replace('"', "").replace('[', "").replace(']', "")]
            arr = []
            for i in range(len(tmp)):
                arr += [[]]
                for j in tmp[i].split():
                    if j.isdigit():
                        arr[i] += [int(j)]

            print(arr)
            polet = response.text
            clear_polet = ''
            for i in range(len(polet)):
                if not (polet[i] == '{' or polet[i] == '}' or polet[i] == ']' or polet[i] == '['):
                    clear_polet += polet[i]
            clear_polet = clear_polet.split('"points":')[1:]

            context['polet'] = polet
            print(all_time(4, 15))

        else:
            context['polet'] = "Неверный токен"

    else:
        form = TokenForm()

    context['form'] = form


    return render(request, 'index.html', context)