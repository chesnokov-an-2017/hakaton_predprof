from django import forms


class TokenForm(forms.Form):
    token = forms.CharField(
        label='Введите токен',
        max_length=8,
        min_length=8,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'style': 'width:100px'
            }
        )
    )
    number = forms.IntegerField(
        label='Введите номер полёта',
        max_value=10,
        min_value=1,
        widget=forms.NumberInput(
            attrs={
                'class': 'form-control',
                'style': 'width:100px'
            }
        )
    )